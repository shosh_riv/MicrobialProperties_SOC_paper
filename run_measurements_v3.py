# -*- coding: utf-8 -*-
"""
Created on Mon Dec 02 09:11:30 2019

@author: cg26ryzi
"""

from glob import glob
import arcpy
from arcpy.sa import *

# Check out any necessary licenses ####
arcpy.CheckOutExtension("spatial")
arcpy.env.overwriteOutput = True

root_folder = r'I:\eie\==PERSONAL\LUCAS_Clean\Maps\Scaled'
root_folder_ann = r'I:\eie\==PERSONAL\LUCAS_Clean\ForMahalanoubisDistance\Resampled'
results_f = r'I:\eie\==PERSONAL\LUCAS_Clean\Maps\Results'

annual_prec = root_folder_ann + '\\MAP_resample.tif'
annual_temp = root_folder_ann + '\\MAT_resample.tif'
elevation = root_folder_ann + '\\elev_resample.tif'
latitude = root_folder_ann + '\\Latitude_scaled.tif'
ph = root_folder_ann + '\\pH_resample.tif'
sand = root_folder_ann + '\\sand_resample.tif'
soc = root_folder_ann + '\\SOC_resample.tif'

forest = r'I:\eie\==PERSONAL\LUCAS_Clean\Maps\Scaled\forest_1km_LAEA_EU.tif'
agriculture = r'I:\eie\==PERSONAL\LUCAS_Clean\Maps\Scaled\agriculture_1km_LAEA_EU.tif'
grassland = r'I:\eie\==PERSONAL\LUCAS_Clean\Maps\Scaled\grassland_1km_LAEA_EU.tif'

list_months = []

f_temp = root_folder + '\\MonthlyTemp'
f_list = glob(f_temp + '\\' + '*.tif')

for i in f_list:
    month = i.split('\\')[-1].split('.')[0]
    list_months.append(month)

for m in list_months:
    print m
    
    temp_m = glob(f_temp + '\\' + m + '*.tif')[0]
    f_prec = root_folder + '\\MonthlyPrecip'
    prec_m = glob(f_prec + '\\' + m + '*.tif')[0]
    
    # General Model
    print 'General Model'
    
    water_contnt = (-0.001 + 
                    (Raster(elevation) * 0.111) +
                    (Raster(latitude) * 0.369) +
                    (Raster(temp_m) * -0.092) +
                    (Raster(prec_m) * 0.110) +
                    (Raster(annual_prec) * -0.066) +
                    (Raster(annual_temp) * 0.192) +
                    (Raster(sand) * -0.188) +
                    (Raster(soc) * 0.600))
    
    water_contnt_pred = results_f + '\\Months\\' + 'water_contnt_g_' + m + '.tif'
    water_contnt.save(water_contnt_pred)
    
    bas = (0.004 +
            (Raster(water_contnt_pred) * 0.344) +
            (Raster(elevation) * -0.189) +
            (Raster(latitude) * -0.387) +
            (Raster(temp_m) * -0.076) +
            (Raster(prec_m) * 0.087) +
            (Raster(annual_temp) * -0.478) +
            (Raster(annual_prec) * 0.141) +
            (Raster(sand) * -0.020) +
            (Raster(ph) * -0.100))
    bas_pred = results_f + '\\Months\\' + 'bas_g_' + m + '.tif'
    bas.save(bas_pred)
    
    Cmic = (0.001 +
            (Raster(water_contnt_pred) * 0.371) +
            (Raster(elevation) * 0.307) +
            (Raster(latitude) * 0.380) +
            (Raster(temp_m) * -0.029) +
            (Raster(prec_m) * -0.090) +
            (Raster(annual_temp) * 0.157) +
            (Raster(annual_prec) * 0.217) +
            (Raster(sand) * -0.257) +
            (Raster(ph) * 0.199))
    Cmic_pred = results_f + '\\Months\\' + 'Cmic_g_' + m + '.tif'
    Cmic.save(Cmic_pred)
   
    # Define list variables
    water_list =[]
    Cmic_list = []
    bas_list = []
    
    # Forest Model
    print 'Forest Model'
    water_contnt_for = (-0.093 + 
                        (Raster(elevation) * -0.299) +
                        (Raster(latitude) * -0.360) +
                        (Raster(temp_m) * -0.195) +
                        (Raster(prec_m) * 0.068) +
                        (Raster(annual_prec) * -0.015) +
                        (Raster(annual_temp) * -0.579) +
                        (Raster(sand) * -0.338) +
                        (Raster(soc) * 0.669)) * Raster(forest)
    
    water_contnt_for_pred = results_f + '\\Months\\' + 'water_contnt_for_' + m + '.tif'
    water_contnt_for.save(water_contnt_for_pred)
    water_list.append(water_contnt_for_pred)
    
    bas_for = (0.248 +
            (Raster(water_contnt_for_pred) * 0.250) +
            (Raster(elevation) * -0.008) +
            (Raster(latitude) * -0.012) +
            (Raster(temp_m) * -0.021) +
            (Raster(prec_m) * 0.042) +
            (Raster(annual_temp) * -0.130) +
            (Raster(annual_prec) * 0.102) +
            (Raster(sand) * -0.073) +
            (Raster(ph) * -0.036)) * Raster(forest)
    
    bas_for_pred = results_f + '\\Months\\' + 'bas_for_' + m + '.tif'
    bas_for.save(bas_for_pred)
    bas_list.append(bas_for_pred)
    
    Cmic_for = (0.271 +
            (Raster(water_contnt_for_pred) * 0.511) +
            (Raster(elevation) * 0.293) +
            (Raster(latitude) * 0.291) +
            (Raster(temp_m) * 0.042) +
            (Raster(prec_m) * -0.042) +
            (Raster(annual_temp) * 0.169) +
            (Raster(annual_prec) * 0.051) +
            (Raster(sand) * -0.225) +
            (Raster(ph) * 0.341)) * Raster(forest)
    
    Cmic_for_pred = results_f + '\\Months\\' + 'Cmic_for_' + m + '.tif'
    Cmic_for.save(Cmic_for_pred)
    Cmic_list.append(Cmic_for_pred)
    
    # Agriculture Model
    print 'Agriculture Model'
    water_contnt_agri = (-0.309 + 
                        (Raster(elevation) * -0.012) +
                        (Raster(latitude) * 0.081) +
                        (Raster(temp_m) * -0.097) +
                        (Raster(prec_m) * 0.105) +
                        (Raster(annual_prec) * -0.017) +
                        (Raster(annual_temp) * 0.070) +
                        (Raster(sand) * -0.109) +
                        (Raster(soc) * 0.174)) * Raster(agriculture)
    
    water_contnt_agri_pred = results_f + '\\Months\\' + 'water_contnt_agri_' + m + '.tif'
    water_contnt_agri.save(water_contnt_agri_pred)
    water_list.append(water_contnt_agri_pred)

    
    bas_agri = (0.086 +
            (Raster(water_contnt_agri_pred) * 1.124) +
            (Raster(elevation) * -0.389) +
            (Raster(latitude) * -0.569) +
            (Raster(temp_m) * -0.028) +
            (Raster(prec_m) * 0.054) +
            (Raster(annual_temp) * -0.519) +
            (Raster(annual_prec) * 0.076) +
            (Raster(sand) * 0.035) +
            (Raster(ph) * -0.099)) * Raster(agriculture)
    
    bas_agri_pred = results_f + '\\Months\\' + 'bas_agri_' + m + '.tif'
    bas_agri.save(bas_agri_pred)
    bas_list.append(bas_agri_pred)
    
    Cmic_agri = (-0.365 +
            (Raster(water_contnt_agri_pred) * 0.190) +
            (Raster(elevation) * 0.277) +
            (Raster(latitude) * 0.457) +
            (Raster(temp_m) * -0.123) +
            (Raster(prec_m) * -0.115) +
            (Raster(annual_temp) * 0.370) +
            (Raster(annual_prec) * 0.358) +
            (Raster(sand) * -0.229) +
            (Raster(ph) * 0.407)) * Raster(agriculture)
    
    Cmic_agri_pred = results_f + '\\Months\\' + 'Cmic_agri_' + m + '.tif'
    Cmic_agri.save(Cmic_agri_pred)
    Cmic_list.append(Cmic_agri_pred)
    
    # Grassland Model
    print 'Grassland Model'
    water_contnt_grass = (-0.024 + 
                        (Raster(elevation) * 0.074) +
                        (Raster(latitude) * 0.162) +
                        (Raster(temp_m) * -0.057) +
                        (Raster(prec_m) * 0.120) +
                        (Raster(annual_prec) * -0.002) +
                        (Raster(annual_temp) * 0.112) +
                        (Raster(sand) * -0.063) +
                        (Raster(soc) * 0.629)) * Raster(grassland)
    
    water_contnt_grass_pred = results_f + '\\Months\\' + 'water_contnt_grass_' + m + '.tif'
    water_contnt_grass.save(water_contnt_grass_pred)
    water_list.append(water_contnt_grass_pred)
    
    bas_grass = (0.175 +
            (Raster(water_contnt_grass_pred) * 0.458) +
            (Raster(elevation) * 0.140) +
            (Raster(latitude) * 0.227) +
            (Raster(temp_m) * -0.019) +
            (Raster(prec_m) * 0.026) +
            (Raster(annual_temp) * 0.069) +
            (Raster(annual_prec) * 0.099) +
            (Raster(sand) * -0.004) +
            (Raster(ph) * 0.044)) * Raster(grassland)
    
    bas_grass_pred = results_f + '\\Months\\' + 'bas_grass_' + m + '.tif'
    bas_grass.save(bas_grass_pred)
    bas_list.append(bas_grass_pred)
    
    Cmic_grass = (0.225 +
            (Raster(water_contnt_grass_pred) * 0.072) +
            (Raster(elevation) * 0.124) +
            (Raster(latitude) * 0.174) +
            (Raster(temp_m) * 0.083) +
            (Raster(prec_m) * -0.073) +
            (Raster(annual_temp) * -0.198) +
            (Raster(annual_prec) * 0.260) +
            (Raster(sand) * -0.281) +
            (Raster(ph) * 0.258)) * Raster(grassland)
    
    Cmic_grass_pred = results_f + '\\Months\\' + 'Cmic_grass_' + m + '.tif'
    Cmic_grass.save(Cmic_grass_pred)
    Cmic_list.append(Cmic_grass_pred)
    
    print 'Computing Europe'
    # Computing Cmic
    Output = results_f + '\\Months\\' + 'Cmic_lu_' + m + '.tif'
    arcpy.gp.CellStatistics_sa(Cmic_list, Output, "MEAN", "DATA")
    
    # Computing bas
    Output = results_f + '\\Months\\' + 'bas_lu_' + m + '.tif'
    arcpy.gp.CellStatistics_sa(bas_list, Output, "MEAN", "DATA")
    
    # Computing water content
    Output = results_f + '\\Months\\' + 'water_contnt_lu_' + m + '.tif'
    arcpy.gp.CellStatistics_sa(water_list, Output, "MEAN", "DATA")
    

list_measures = ['water_contnt', 'bas', 'Cmic']

# Compute annual mean and SD for the land cover model
for l in list_measures:
    l_files = glob(results_f + '\\Months\\' + l + '_lu_' + '*.tif')
    Output = results_f + '\\' + l + '_lu_annual.tif'
    arcpy.gp.CellStatistics_sa(l_files, Output, "MEAN", "DATA")

for l in list_measures:
    l_files = glob(results_f + '\\Months\\' + l + '_lu_' + '*.tif')
    Output = results_f + '\\' + l + '_lu_annual_SD.tif'
    arcpy.gp.CellStatistics_sa(l_files, Output, "STD", "DATA")

# Compute annual mean and SD for the general model
for l in list_measures:
    l_files = glob(results_f + '\\Months\\' + l + '_g_' + '*.tif')
    Output = results_f + '\\' + l + '_g_annual.tif'
    arcpy.gp.CellStatistics_sa(l_files, Output, "MEAN", "DATA")

for l in list_measures:
    l_files = glob(results_f + '\\Months\\' + l + '_g_' + '*.tif')
    Output = results_f + '\\' + l + '_g_annual_SD.tif'
    arcpy.gp.CellStatistics_sa(l_files, Output, "STD", "DATA")




